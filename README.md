#Enclosureproductions site


##Info

https://www.enclosureproductions.nl

##Lokaal starten

        > php artisan serve
        
##Theming

+ Gebaseerd op bootstrap
+ Builden

        > npm install
        > npm run watch
        
## Settings

In .env:

+ GALLERY_DIR directory for images for photo gallery.
+ GALLERY_THUMB_DIR directory for thumbnail images for photo gallery.
+ ALLOWED_IPS Pipe-separated ('|') ip address form where log in is possible.