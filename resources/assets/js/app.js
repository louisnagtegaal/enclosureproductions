
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});

$(document).ready(function () {
    init();

    function init(){
        var current = 0;
        var audio = $('#audioPlayer');
        var playlist = $('#playlist');
        var tracks = playlist.find('li a');
        var len = tracks.length - 1;
        if(audio.length != 0) {
            audio[0].volume = .50;
            audio[0].play();

            playlist.on('click', 'a', function (e) {
                e.preventDefault();
                link = $(this);
                current = link.parent().index();
                run(link, audio[0]);
            });

            audio[0].addEventListener('ended', function (e) {
                current++;
                if (current == len) {
                    current = 0;
                    link = playlist.find('a')[0];
                } else {
                    link = playlist.find('a')[current];
                }
                run($(link), audio[0]);
            });
        }

        var gallerySelected = $('#gallerySelected');
        var thumbTimer;

        if(gallerySelected.length != 0) {
            thumbTimer = setTimeout(function () {
                highlight();
            }, 2000);

            $('#thumbslist').on('click', 'a', function (e) {
                e.preventDefault();
                gallerySelected.removeClass('active');
                var img = $(this).attr('data-url');
                var path = '/images/photos/' + img;
                gallerySelected.attr("src", path);
                gallerySelected.addClass('active');
            });

            var timer;
            $('#thumbslist').on('mouseenter', 'a', function (e) {
                var img = $(this).attr('data-url');
                var path = '/images/photos/' + img;
                timer = setTimeout(function () {
                    gallerySelected.attr("src", path);
                    gallerySelected.addClass('active');
                }, 100);
                }).on("mouseleave", "a", function(){
                clearTimeout(timer);
            });
        }
    }

    function highlight(){
        $("#thumbslist li a img").removeClass("active");
        var thumbCount = $('#thumbslist li').length;
        var itm = Math.floor((Math.random() * thumbCount) + 1);
        var selector = '#thumb-' + itm;
        $(selector).addClass('active');;
        highlightRepeat();
    }

    function highlightRepeat(){
        thumbTimer = setTimeout(function () {
            highlight();
        }, 500);
    }

    function run(link, player){
        player.src = link.attr('href');
        par = link.parent();
        par.addClass('active').siblings().removeClass('active');
        player.load();
        player.play();
    }
});