@extends('layouts.app')
@section('content')
    <div class="content-text gallery">
        <div class="row">
            <div class="main-image col-md-8 order-md-2">
                <img src="{{ $first }}" class="gallery-full" id="gallerySelected">
            </div>
            <div class="gallery-thumbs col-md-4 order-md-1">
                <ul id="thumbslist" class="thumbslist">
                    @foreach ($thumbs as $base => $thumb)
                        <li class="thumb">
                            <a href="#" data-url="{{ $base }}">
                                <img src="{{ $thumb }}" id="thumb-{{$loop->index}}" class="gallery-thumb">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @if(isset($articles))
            <div class=row">
                <div class="col-md-12">
                    @include('components/intro')
                </div>
            </div>
        @endif
    </div>
@endsection
