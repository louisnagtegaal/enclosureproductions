@include('components/header')
<body class="body {{ @Route::currentRouteName() }}">
<div id="app">
    <div class="container">
        <div class="title m-b-md">
            Enclosure Prods.
            <div class="payoff m-b-md">
                <div class="content">Est. 1979</div>
            </div>
        </div>
    </div>
    @include('components/navigation')
    <div class="container">
        @if(isset($dirnav_array))
            @include('components/dirnav')
        @endif
        <div class="row">
            <main class="content @if(isset($nav_array)) col-md-8 justify-content-md-end @else col-md-12 @endif">
                @yield('content')
            </main>
            @if(isset($nav_array))
                @include('components/subnav')
            @endif
        </div>
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
