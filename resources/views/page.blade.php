@extends('layouts.app')
@section('content')
    @if(isset($articles))
        @include('components/intro')
    @endif
    <div class="content-text">
        @if(!empty($article))
            <h2>{{ $article->title }}</h2>
            <span class="body">{!! $article->body !!}</span>
        @endif
    </div>
@endsection
