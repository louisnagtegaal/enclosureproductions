@extends('layouts.app')

@section('content')
    <h1>Not found</h1>
    <span class="body">Maybe try something else?</span>
@endsection
