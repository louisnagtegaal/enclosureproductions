<nav class="subnavbar col-md-4 justify-content-md-start">
    <ul class="navbar-nav">
        @foreach ($nav_array as $title => $url)
            <li>
                @if(is_null($url))
                    <span class="current">{{ $title }}</span>
                @else
                    <a class="nav-link" href="/{{ $url }}">{{ $title }}</a>
                @endif
            </li>
        @endforeach
    </ul>
</nav>
