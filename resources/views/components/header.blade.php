<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="Description" CONTENT="Official site of Enclosure productions, publisher of The Gentry albums and art by Louis Nagtegaal.">
    <meta name="google-site-verification" content="QiZYvwz4XmEkHehl_FfS7rFTSoeK5F8GiBRwO8JwTEA" />
    <title>{{ config('app.name', 'Enclosure Prods.') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!--Dynamic StyleSheets added from a view would be pasted here-->
    @stack('styles')
</head>
