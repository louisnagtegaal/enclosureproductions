<div class="row">
    <nav class="dirnavbar navbar-light col-md-4 justify-content-md-start">
        <ul class="navbar-nav dir-nav">
            @foreach ($dirnav_array as $title => $url)
                <li>
                    @if(is_null($url))
                        <span class="current">{{ $title }}</span>
                    @else
                        <a class="nav-link" href="/{{ $url }}">{{ $title }}</a>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>
</div>