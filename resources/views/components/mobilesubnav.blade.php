<ul class="navbar-nav mobilesubnavbar">
    @foreach ($nav_array as $title => $url)
        @if(is_null($url))
            <span class="current">{{ $title }}</span>
        @else
            @if(!empty($title))
                <a class="nav-link" href="/{{ $url }}">{{ $title }}</a>
            @endif
        @endif
    @endforeach
</ul>

