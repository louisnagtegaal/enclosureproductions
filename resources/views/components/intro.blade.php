<div class="content-text">
    @foreach ($articles as $article)
        <h3>{{$article->title}}</h3>
        <div class="row">
            <div @if(!empty($article->mainimage))
                 class="col-md-4"
                 @else
                 class="col-md-8"
                    @endif>{!! $article->body !!}</div>
            @if(!empty($article->mainimage))
                <div class="col-md-8 main-image"><img src="{{ $article->mainimage }}"></div>
            @endif
        </div>
    @endforeach
</div>