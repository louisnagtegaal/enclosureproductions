<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">&nbsp;</span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Navbar -->
            <ul class="navbar-nav">
                <li><a class="navbar-brand mobile @if(Request::url() === url('/')) active @endif" href="{{ url('/') }}">
                        Welcome
                    </a></li>
                <li><a class="navbar-brand mobile @if(Request::url() ===  url('blogs') || Request::is('blog/*')) active @endif" href="{{ url('blogs') }}">
                        Blogs
                    </a>
                    @if(Request::url() ===  url('blogs') || Request::is('blog/*'))
                        @if(isset($nav_array))
                            @include('components/mobilesubnav')
                        @endif
                    @endif</li>
                <li><a class="navbar-brand mobile @if(Request::url() ===  url('albums') || Request::is('album/*')) active @endif" href="{{ url('albums') }}">
                        Music
                    </a>
                    @if(Request::url() ===  url('albums') || Request::is('album/*'))
                        @if(isset($nav_array))
                            @include('components/mobilesubnav')
                        @endif
                    @endif</li>
                <li><a class="navbar-brand mobile @if(Request::url() ===  url('gallery') ) active @endif" href="{{ url('gallery') }}">
                        Photography
                    </a>
                    @if(Request::url() ===  url('gallery'))
                        @if(isset($nav_array))
                            @include('components/mobilesubnav')
                        @endif
                    @endif</li>
            </ul>
            <ul class="navbar-nav user-menu">
               @include('components/auth')
            </ul>
        </div>
    </div>
</nav>
