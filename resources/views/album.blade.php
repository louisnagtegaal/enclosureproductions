@extends('layouts.app')
@section('content')
    <div class="content-text album">
        <h2>{{ $article->title }} ({{ $article->year}})</h2>
        <span class="player">
             <audio id="audioPlayer" preload="auto" controls></audio>
            <ul id="playlist" class="playlist">
                @foreach ($playlist as $id => $data)
                    <li><a href="{{ $data['uri'] }}">{{ $data['title'] }}</a></li>
                @endforeach
            </ul>
        </span>
        <span class="albumart">
            @if($article->cover)
                <img src="/{{ $article->cover }}">
            @endif
        </span>
        <span class="albumtext">{!! $article->text !!}</span>
    </div>
@endsection
