<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/blogs', 'BlogController@index')->name('blogs');
Route::get('/blog/{slug}', 'BlogController@show')->name('blog');
Route::get('/albums', 'AlbumController@index')->name('albums');
Route::get('/album/{slug}', 'AlbumController@show')->name('album');
Route::get('/gallery', 'GalleryController@index')->name('gallery');
Route::get('/gallery/{base}', 'GalleryController@show')->name('galleryitem');
Route::get('/uploadindex', 'MarkdownController@index')->name('uploadsindex')->middleware('auth');
Route::get('/uploadblog', 'MarkdownController@upload')->name('uploadblog')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
Route::get('500',['as'=>'500','uses'=>'ErrorHandlerController@errorCode500']);
Route::group(['middleware' => 'ipcheck'], function () {
    Auth::routes();
});
