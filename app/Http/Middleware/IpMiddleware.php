<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{
    /**
     * Check originating ip address of an incoming request. Used to disallowd login-functions.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get list of allowd ip's from env.
        $allowed_ip_env = env('ALLOWED_IPS', '127.0.0.1');
        // And explode it to array.
        $allowed_ip = explode('|', $allowed_ip_env);
        // Check if current request originates from allowed ip.
        if (!in_array($request->ip(), $allowed_ip)) {
            // If not, redirect to home.
            return redirect('/');
        }
        return $next($request);
    }
}
