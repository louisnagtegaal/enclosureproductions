<?php

namespace App\Http\Controllers;

use App\Article;
use \Imagick;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GalleryController extends Controller
{

    protected $gallery_dir;
    protected $thumb_dir;

    function __construct()
    {
        $this->gallery_dir = getcwd() . env('GALLERY_DIR', '/images/photos');
        $this->thumb_dir = getcwd() . env('GALLERY_THUMB_DIR', '/images/photos/thumbs');
    }

    public function index()
    {
        $articles = Article::orderBy('order')->where('route', 'gallery')->get();
        $thumbs = $this->getThumbs();
        $subnav = $this->getSubnav();
        $first_photo = reset($thumbs);
        $first = env('GALLERY_DIR', '/images/photos') . '/' . basename($first_photo);
        return view('gallery', ['thumbs' => $thumbs, 'first' => $first, 'articles' => $articles, 'dirnav_array' => $subnav]);
    }

    public function show($base)
    {
        $articles = Article::orderBy('order')->where('route', 'gallery')->get();
        if (!empty($base)) {
            $base = '/' . $base;
        }
        $thumbs = $this->getThumbs($base);
        $subnav = $this->getSubnav($base);
        $first_photo = reset($thumbs);
        $first = env('GALLERY_DIR', '/images/photos') . $base . '/' . basename($first_photo);
        return view('gallery', ['thumbs' => $thumbs, 'first' => $first, 'articles' => $articles, 'dirnav_array' => $subnav]);
    }

    protected function getThumbs($base = '')
    {
        $thumbs = [];
        $baseDir = $this->gallery_dir . $base;
        if (is_dir($baseDir)) {

            $thumbBase = $this->thumb_dir . $base;
            if (!file_exists($thumbBase)) {
                mkdir($thumbBase, 0777, true);
            }
            foreach (glob($baseDir . '/*.*') as $file) {
                $baseFile = basename($file);
                $thumb_path = $thumbBase . '/' . $baseFile;
                if (!file_exists($thumb_path)) {
                    $this->createThumb($file, $thumb_path);
                }
                $thumbUrl = ltrim($base . '/' . $baseFile, '/');
                $thumbs[$thumbUrl] = env('GALLERY_THUMB_DIR', '/images/photos/thumbs') . $base . '/' . $baseFile;
            }

            $thumbs = $this->shuffle_assoc($thumbs);
            return $thumbs;
        } else {
            throw new NotFoundHttpException();
        }
    }

    protected function getSubnav($base = '')
    {
        $nav_array = array();
        $baseDir = $this->gallery_dir . $base;
        if (is_dir($baseDir)) {
            if ($base === '') {
                $gallery_text = "Gallery";
            } else {
                $gallery_text = "Gallery ↩";
            }
            $nav_array[$gallery_text] = 'gallery';
            $subs = array_filter(glob($baseDir . '/*'), 'is_dir');
            foreach ($subs as $sub) {
                $subpath = ltrim(str_replace($this->gallery_dir, '', $sub), '/');
                if ($subpath !== 'thumbs') {
                    $nav_array[$subpath] = 'gallery/' . $subpath;
                }
            }
            return $nav_array;
        } else {
            throw new NotFoundHttpException();
        }
    }

    protected function createThumb($file, $thumb_path)
    {
        $imagick = new \Imagick($file);
        if ($imagick) {
            $imagick->setbackgroundcolor('rgb(0, 0, 0)');
            $imagick->thumbnailImage(30, 30, true, true);
            $imagick->writeImage($thumb_path);
        }
    }

    protected function shuffle_assoc($list)
    {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key)
            $random[$key] = $list[$key];

        return $random;
    }
}