<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Article;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AlbumController extends Controller {

  public function index()
  {
	$nav_array = $this->getLinks();
	$articles = Article::orderBy('order')->where('route', 'albums')->get();
	return view('page', ['nav_array' => $nav_array, 'articles' => $articles]);
  }


  public function show($slug)
  {
	$nav_array = $this->getLinks($slug);
	$album = Album::findBySlug($slug);
	if (!empty($album)) {
	  $playlist = $this->getPlayList($album->id);
	  return view('album', [
		'nav_array' => $nav_array,
		'article' => $album,
		'playlist' => $playlist
	  ]);
	}
	else {
	  throw new NotFoundHttpException();
	}
  }

  protected function getLinks($slug = '')
  {
	$albums = Album::orderBy('year', 'asc')->get();
	$nav_array = [];
	foreach ($albums as $album) {
	  // Update slugs if necessary
	  if (empty($album->slug)) {
		$album->save();
	  }
	  if ($album->slug == $slug) {
		$nav_array[$album->title] = null;
	  }
	  else {
		$nav_array[$album->title] = 'album/' . $album->slug;
	  }
	}
	return $nav_array;
  }

  protected function getPlayList($id)
  {
	$items = [];
	$songs = Album::find($id)->songs;
	foreach ($songs as $song) {
	  $items[$song->id] = [
		'title' => $song->title,
		'uri' => '/' . $song->uri,
	  ];
	}
	return $items;
  }
}
