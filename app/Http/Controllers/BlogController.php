<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Article;

class BlogController extends Controller
{
    public function index()
    {
        $nav_array = $this->getLinks();
        //Check if any artile is associated with this route
        $articles = Article::orderBy('order')->where('route', 'blogs')->get();
        return view('page', ['nav_array' => $nav_array, 'articles' => $articles]);
    }

    public function show($slug){
        $nav_array = $this->getLinks($slug);
        $blog = Blog::findBySlug($slug);
        if(!empty($album)) {
            return view('page', ['nav_array' => $nav_array, 'article' => $blog]);
        }
        else{
            throw new NotFoundHttpException();
        }
    }

    protected function getLinks($slug=''){
        $blogs = Blog::all();
        $nav_array = [];
        foreach($blogs as $blog){
            // Update slugs if necessary
            if(empty($blog->slug)) {
                $blog->save();
            }
            if($blog->slug==$slug){
                $nav_array[$blog->title] = null;
            }
            else{
                $nav_array[$blog->title] = 'blog/' . $blog->slug;
            }
        }
        return $nav_array;
    }
}
