<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;


class Album extends Model
{
	use Sluggable;
	use SluggableScopeHelpers;
	/**
	 * Get the songs for the album.
	 */
	public function songs()
	{
		return $this->hasMany('App\Song');
	}

	/**
	 * Return the sluggable configuration array for this model.
	 *
	 * @return array
	 */
	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}
}
