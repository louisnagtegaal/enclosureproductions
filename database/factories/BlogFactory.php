<?php

use Faker\Generator as Faker;

$factory->define(App\Blog::class, function (Faker $faker) {
    return [
        'title' => substr($faker->sentence(2), 0, -1),
        'body' => $faker->paragraph,
    ];
});
