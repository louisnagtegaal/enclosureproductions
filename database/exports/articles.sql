
TRUNCATE TABLE `articles`;
INSERT INTO `articles` (`id`, `created_at`, `updated_at`, `title`, `body`, `mainimage`, `route`, `order`) VALUES
(1, '2018-02-20 22:00:00', '2018-02-20 22:00:00', 'Welcome to Enclosure Prods.', 'Enclosure productions came to live at the end of the 70\'s and initially was used just as a nickname for the mail/art project of the brothers Paul (<em>Dirty Rat Inc.</em>) and Louis (<em>Enclosure prods.</em>) Nagtegaal.<br/>\r\nWhen in the 80\'s the first album of <em>The Gentry</em> was released, a name for a production company was necessary and so <em>Enclosure Prods.</em> was used for this and all later releases of <em>The Gentry</em>.', 'images/covers/EensgezindheidHymn.jpg', 'welcome', 1),
(2, '2018-02-20 22:00:00', '2018-02-20 22:00:00', 'Blogs by Louis Nagtegaal', 'Here you can find a number of blogs written by me. ', '', 'blogs', 1),
(3, '2018-02-21 23:00:00', '2018-02-21 23:00:00', 'Music', '<em>Enclosure prods.</em> released a number of albums from <em>The Gentry</em> and its predecessor <em>De Eensgezindheid</em><br/>\r\nMost albums can be listened to in full here.<br/><br/>\r\nThe artwork for the albums was also created by <em>Enclosure Prods.</em> with exception of the artwork for the latest album <em>On the margins of essence</em> with was created by <em>Jennifer Keek</em> based on photos made by <em>Louis Nagtegaal</em>', '', 'albums', 1);

