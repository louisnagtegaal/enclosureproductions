TRUNCATE TABLE `albums`;
--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `created_at`, `updated_at`, `title`, `text`, `cover`, `year`) VALUES
(1, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Fragments Of Truth', '', 'images/covers/FragmentsOfTruth.jpg', 1984),
(2, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Solitary', '', 'images/covers/Solitary.jpg', 1986),
(4, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'On The Margins Of Essence', '', 'images/covers/OnTheMarginsOfEssence.jpg', 2010),
(5, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Growing up absurd', '', 'images/covers/GrowingUpAbsurd.jpg', 1987),
(6, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'De Eensgezindheid : Hymn', 'Before \'The Gentry\' was formed Laurens and Luis played in a band that was called \'De Eensgezindheid\'.', 'images/covers/EensgezindheidHymn.jpg', 1981),
(7, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Zaanradio', '', 'images/covers/ZaanRadio.jpg', 2011);


