--
-- Dumping data for table `songs`
--

TRUNCATE TABLE `songs`;

INSERT INTO `songs` (`id`, `created_at`, `updated_at`, `title`, `text`, `uri`, `album_id`, `album_order`) VALUES
(1, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Agree with the public opion', '', 'audio/Eensgezindheid/01.mp3', 6, 1),
(2, '2018-02-19 23:00:00', '2018-02-19 23:00:00', '20 minutes', '', 'audio/Eensgezindheid/02.mp3', 6, 2),
(3, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Dark holes', '', 'audio/Eensgezindheid/03.mp3', 6, 3),
(4, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Samba for an ostrich', '', 'audio/Eensgezindheid/04.mp3', 6, 4),
(5, '2018-02-19 23:00:00', '2018-02-19 23:00:00', '24 Pieces', '', 'audio/Eensgezindheid/05.mp3', 6, 5),
(6, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'In those days', '', 'audio/Eensgezindheid/06.mp3', 6, 6),
(7, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Waiting for death', '', 'audio/Eensgezindheid/07.mp3', 6, 7),
(8, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Living in shelters', '', 'audio/Eensgezindheid/08.mp3', 6, 8),
(9, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Principals', '', 'audio/Eensgezindheid/09.mp3', 6, 9),
(10, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'High buildings', '', 'audio/Eensgezindheid/10.mp3', 6, 10),
(11, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'All the faceless people', '', 'audio/Fragments/01.mp3', 1, 1),
(12, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Mortal ride', '', 'audio/Fragments/02.mp3', 1, 2),
(13, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'The treat', '', 'audio/Fragments/03.mp3', 1, 3),
(14, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Fragments of truth', '', 'audio/Fragments/04.mp3', 1, 4),
(15, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'After the fact', '', 'audio/Fragments/05.mp3', 1, 5),
(16, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Indifference', '', 'audio/Fragments/06.mp3', 1, 6),
(17, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Escape', '', 'audio/Fragments/07.mp3', 1, 7),
(18, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Senses', '', 'audio/Fragments/08.mp3', 1, 8),
(19, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Too many of you', '', 'audio/Fragments/09.mp3', 1, 9),
(20, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Visions of a landscape', '', 'audio/Fragments/10.mp3', 1, 10),
(21, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Mutual Distrust', '', 'audio/Solitary/01.mp3', 2, 1),
(22, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Colour of Darkness', '', 'audio/Solitary/02.mp3', 2, 2),
(23, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Desperate Day', '', 'audio/Solitary/03.mp3', 2, 3),
(24, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'The Desert', '', 'audio/Solitary/04.mp3', 2, 4),
(25, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Grey and Timeless', '', 'audio/Solitary/05.mp3', 2, 5),
(26, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Into Fears', '', 'audio/Solitary/06.mp3', 2, 6),
(27, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Suburban Rain', '', 'audio/Solitary/07.mp3', 2, 7),
(28, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Renewal', '', 'audio/Solitary/08.mp3', 2, 8),
(29, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'A Deathlike Dream', '', 'audio/Solitary/09.mp3', 2, 9),
(30, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Away from the Millions', '', 'audio/Solitary/10.mp3', 2, 10),
(31, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Things fall apart', '', 'audio/Solitary/11.mp3', 2, 11),
(32, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'La Chute', '', 'audio/Growing/01.mp3', 5, 1),
(33, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Growing up absurd', '', 'audio/Growing/02.mp3', 5, 2),
(34, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Distant view', '', 'audio/Growing/03.mp3', 5, 3),
(35, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Justice without trial', '', 'audio/Growing/04.mp3', 5, 4),
(36, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Diminished responsibilities', '', 'audio/Growing/05.mp3', 5, 5),
(37, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'The homecoming', '', 'audio/Growing/06.mp3', 5, 6),
(38, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Wanna come', '', 'audio/Growing/07.mp3', 5, 7),
(39, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Uncertainty', '', 'audio/Growing/08.mp3', 5, 8),
(40, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'The waste', '', 'audio/Growing/09.mp3', 5, 9),
(41, '2018-02-19 23:00:00', '2018-02-19 23:00:00', 'Halfway', '', 'audio/Growing/10.mp3', 5, 10);


